# Devcontainers Templates - terraform-gcp

This project is a [Devcontainer](.devcontainer/) template built against
`alpine`, `debian` and `ubuntu` images of the
[GCloud CLI template](https://gitlab.com/geekstuff.dev/devcontainers/templates/gcloud-cli).

It adds Terraform, Tfswitch and their auto-complete integrations.

## How it's used

The .devcontainer/ configuration in this project can be used with simple entries
in your own `.devcontainer/devcontainer.json` file where you can further customize
it for example:

```json
{
    "name": "my devcontainer",
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/terraform-gcp/alpine/3.17",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/docker": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/terraform": {
            "version": "1.3.7"
        },
        "ghcr.io/geekstuff-dev/devcontainers-features/vault-cli": {}
    }
}
```

From the example above:

- In this example, image `alpine/3.17` can be changed to `debian/bullseye` or `ubuntu/20.04`
- Image can also have a tag, `:latest` for the main branch, or `:vX.Y` from tags in this project.
    - See [here for the docker image list](https://gitlab.com/geekstuff.dev/devcontainers/templates/terraform-gcp/container_registry)
- While the image comes with the latest terraform, it can also pin a specific version.
- That example also wanted Vault CLI.
- It also caches VSCode extensions (which is useful but can get corrupted)
