#!/bin/sh

FILE=/usr/local/bin/devcontainer-components

cat <<- "EOF" >> $FILE
#!/bin/sh

echo "> Devcontainer components"
gcloud version | grep 'Google Cloud SDK'
terraform -version | grep 'Terraform v'
EOF
chmod ugo+x ${FILE}
